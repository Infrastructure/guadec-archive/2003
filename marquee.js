//      -- JavaScript Marquee v2.0.5 (BETA) --
//
//        JavaScript Copyright (c) Tarmle 2002
//            tarmle @ ruinsofmorning.net
//
//      Visit www.ruinsofmorning.net for full
//      instructions on installing and running
//      this script on your web pages!
//
// You can use this code on the condition that you
// leave this message intact - If you're going to
// benefit from my hard work at least allow me this.
//
// Cheers.
//
//  - Tarmle


// ----------- SETTINGS -----------


// This array contains your messages. There MUST be AT LEAST two messages.
// You can use HTML in your messages but NOT inline styles (use classes instead).
// ALL apostrophies (') MUST be escaped using a backslash (\').
// To prevent clipping, long lines of text MUST be manually broken using the <BR> tag.

//  var msgarray=new Array
// (
//  '<center><B><p class=headertm>Message One</p></b></center>',
//  '<center><B>Message Two</b></center>',
//  '<center><B><p class=error>Message Three</p></b></center>',
//  '<center><B><a href="/">Message Four</a></b></center>'
// );

// var msgarray = new Array();

// Speed Settings
var pausetime = 6000; // Length of time in miliseconds that the marquee will pause on each message.
var interval = 100;    // Time inteval in miliseconds between each increment in movement (lower = faster but 0 will crash big-time).
var msginc = 20;      // The number of increments for each transition. This is a fraction of the marquee's width/height (lower = faster). 

// Movement Settings
var direction = 'up';     // The direction the marquee should move, valid directions are 'up', 'down', 'left', 'right', 'althoz', 'altvir', 'rand', 'pattern' (see below).
var direc = 'up'          // For direction settings 'althoz' and 'altvir' only - sets _opposite_ of first direction.
var transition = 'contig' // Transition method. Valid selections are 'contig', 'cover', 'uncover', 'alt' (cover/uncover), 'rand', 'pattern' (see below).
var trans = 'cover'       // For transition settings 'alt' only - sets _opposite_ of first transition.

// For direction setting 'pattern' only. The following array lists the pattern of movement i.e. ('left','left','right','right').
var dpat = new Array('up','right','down','left');

// For transition setting 'pattern' only. The following array lists the pattern of transition i.e. ('contig','cover','uncover','cover').
var tpat = new Array('cover','uncover','contig');

// Tie patterns to messages. Set this to true if you want the defined pattern linked to it's relative message (1st message with first direction, etc).
tiepat = false;

// Colour Settings
var bgcolor = 'white';   // This is the background color of the marquee and messages and can use any CSS color value including hex-RGB (#0099FF) - 'transparent' only renders correctly with transition set to 'contig';


//----------- DO NOT EDIT BELOW THIS LINE! -----------


var msgc=0;
var msgcp=1;
var msgn=msgarray.length;
var dpatc=0; var tpatc=0; var dpatm=0; var tpatm=0;
var d=document;

if (tiepat)
{
 var ar = dpat;
 var c = 0;
 while(dpat.length < msgn)
 {
  dpat.push(ar[c]);
  c++;
  if (c >= ar.length) {c=0;}
 }
}

var mrqh=null; var mrqw=null; var mrqt=null; var mrql=null;
var msgh=null; var msgw=null; var msgt=null; var msgpad=0;
var virt=0; var virl=0;

var IID=0;

var appN = navigator.appName; var appV = navigator.appVersion.substring(0,1);
var ie = (appN=="Microsoft Internet Explorer" && appV >= 4) ? true : false;
var ns = (appN=="Netscape" && (appV >= 4 && appV < 5)) ? true : false;
var nsix = (appN=="Netscape" && appV >= 5) ? true : false;
var op = (navigator.userAgent.indexOf('Opera') >= 0) ? true : false;


// ADDED BY THEDOC
function insertmsgs(msgs) {
  msgarray = new Array(msgs);
}

function beginmrq()
{
 if (op) {return;}
 if (ie||ns||nsix)
 {
  setupmrq();
  laymrq();
  laymsg();
  clearInterval(IID);
  pause();
 }
}


function insertdivs()
{
 if (op) {return;}
 if (ie||ns||nsix)
 {
  setupmrq();
  d.write('<div id="marquee" name="marquee" onMouseOver="javascript:clientpause(true);" onMouseOut="javascript:clientpause(false);" class="marquee">')
  for (i=0; i<msgarray.length; i++)
  {
   startmsg='<div id="message'+i+'" name="message'+i+'" class="message"><table width="'+mrqw+'" height="'+mrqh+'" border="0" cellspacing="0" cellpadding="0"><tr><td height="'+mrqh+'" width="'+mrqw+'" align="left" valign="top"><nobr>';
   d.write(startmsg+msgarray[i]+'</nobr></td></tr><tr><td align="center"><img src="spacer.gif" width="'+mrqw+'" height="1"></td></tr></table></div>');
  }
  d.write('</div>');
 }
}

function setupmrq()
{
 if (ie||nsix)
 {
  var mspace=d.mrqspacer;
  mrqt=mspace.offsetTop; mrql=mspace.offsetLeft;
  mrqw=mspace.width; mrqh=mspace.height;
 } else {
  var mspace=d.images.mrqspacer;
  mrqt=mspace.y; mrql=mspace.x;
  mrqw=mspace.width; mrqh=mspace.height;
 }
 msgw=mrqw; msgh=mrqh; msgt=msgh*2;
}

function laymrq()
{
 if (ie||nsix)
 {
  with (d.getElementById('marquee').style)
  {
   top=mrqt-msgh; left=mrql; width=mrqw; height=(mrqh*2);
   clip="rect("+mrqh+"px "+mrqw+"px "+(mrqh*2)+"px 0px)";
   visibility="visible";
  };
 } else {
  with (d.layers.marquee)
  {
   top=mrqt-msgh; left=mrql; width=mrqw; height=(mrqh*2);
   clip.width=mrqw; clip.top=msgh; clip.height=mrqh;
   visibility="show";
  };
 }
}

function laymsg()
{
 for (msgc=0; msgc < msgn; msgc++)
 {
  if (ie||nsix)
  {
   with (d.getElementById("message"+msgc).style)
   {
    width=msgw; height=msgh;
    if (msgc) {top=msgh*2;} else {top=msgh; virt=msgh;}
    left=0;
    clip="rect(0px "+mrqw+"px "+mrqh+"px 0px)";
    visibility="visible";
   }
  } else {
   with (d.layers.marquee.document["message"+msgc])
   {
    clip.width=msgw; clip.height=msgh;
    if (msgc) {top=msgh*2} else {top=msgh};
    visibility="show";
   }
  }
 }
 msgc=0;
}


function timing()
{
 clearInterval(IID);
 IID=setInterval("movemsg()",interval);
}

function pause()
{
 clearInterval(IID);
 TID=setTimeout("clearTimeout(TID);timing()",pausetime);

 laymrq();

 if (direction == 'rand')
 {
  var rn=Math.random()*3;
  rn=Math.round(rn);
  var dar = new Array('up','down','left','right');
  direc = dar[rn];
 }

 if (direction == 'pattern')
 {
  if (tiepat)
  {
   direc = dpat[msgc];
  } else {
   direc = dpat[dpatc];
   dpatc++;
   if (dpatc >= dpat.length) {dpatc = 0;}
  }
 }
 if (direction == 'althoz') {direc = (direc == 'left') ? 'right' : 'left';}
 if (direction == 'altvir') {direc = (direc == 'up') ? 'down' : 'up';}
 if (direction != 'rand' && direction != 'altvir' && direction != 'althoz' && direction != 'pattern')  {direc = direction;}

 if (transition == 'rand')
 {
  var rn=Math.random()*2;
  rn=Math.round(rn);
  var tar = new Array('contig','cover','uncover');
  trans = tar[rn];
 }
 if (transition == 'pattern')
 {
  trans = tpat[tpatc];
  tpatc+=1;
  if (tpatc >= tpat.length) {tpatc = 0;}
 }
 if (transition == 'alt') {trans = (trans == 'cover') ? 'uncover' : 'cover';}
 if (transition != 'rand' && transition != 'alt' && transition != 'pattern')  {trans = transition;}
}

function repos()
{
 clearInterval(IID);
 clearTimeout(TID);
 if (ie||nsix)
 {
  TID=setTimeout("clearTimeout(TID);clearInterval(IID);setupmrq();laymrq();timing()",1500);
 } else {
  window.location.reload();
 }
}


function sett(mn,pos)
{
 if (ie||nsix)
 {
  d.getElementById("message"+mn).style.top=pos;
 } else {
  d.layers.marquee.document["message"+mn].top=pos;
 }
}

function setl(mn,pos)
{
 if (ie||nsix)
 {
  d.getElementById("message"+mn).style.left=pos;
 } else {
  d.layers.marquee.document["message"+mn].left=pos;
 }
}

function setz(mn,zin)
{
 if (ie||nsix)
 {
  d.getElementById("message"+mn).style.zIndex = zin;
 } else {
  d.layers.marquee.document['message'+mn].zIndex = zin;
 }
}

function setv(mn,tf)
{
 if (ie||nsix)
 {
  d.getElementById("message"+mn).style.visibility = (tf) ? 'visible' : 'hidden';
 } else {
  d.layers.marquee.document['message'+mn].visibility = (tf) ? 'show' : 'hide';
 }
}


function movemsg()
{
 if (direc == 'up') {virt -= (msgh/msginc);}
 if (direc == 'down') {virt += (msgh/msginc);}
 if (direc == 'left') {virl -= (msgw/msginc);}
 if (direc == 'right') {virl += (msgw/msginc);}

 setv(msgc,true);

 if (trans == 'contig' || trans == 'uncover')
 {
  if (direc == 'up') {sett(msgc,virt);}
  if (direc == 'left') {setl(msgc,virl);}
  if (direc == 'down') {sett(msgc,virt);}
  if (direc == 'right') {setl(msgc,virl);}
 } else {
  setl(msgc,0); sett(msgc,msgh);
 }

 if (trans == 'uncover') {setz(msgc,2);} else {setz(msgc,0);}

 msgcp=msgc+1;
 if (msgcp >= msgn) {msgcp=0;}

 setv(msgcp,true);

 if (trans == 'contig' || trans == 'cover')
 {
  if (direc == 'up') {sett(msgcp,virt+msgh); setl(msgcp,virl);}
  if (direc == 'left') {setl(msgcp,virl+msgw); sett(msgcp,virt);}
  if (direc == 'down') {sett(msgcp,virt-msgh); setl(msgcp,virl);}
  if (direc == 'right') {setl(msgcp,virl-msgw); sett(msgcp,virt);}
 } else {
  setl(msgcp,0); sett(msgcp,msgh);
 }

 if (trans == 'uncover') {setz(msgcp,0);} else {setz(msgcp,2);}

 stepflag = 0;

 if (direc == 'up' && virt <= 0) { sett(msgc,0); sett(msgcp,msgh); stepflag = 1;}
 if (direc == 'down' && virt >= msgh*2) {sett(msgc,0); stepflag = 1;}
 if (direc == 'left' && virl <= 0-msgw) {setl(msgc,0-msgw); stepflag = 1;}
 if (direc == 'right' && virl >= msgw) {setl(msgc,0-msgw); stepflag = 1;}

 if (stepflag)
 {
  setv(msgc,false);
  virl = 0;
  virt = msgh;
  msgc++;
  if (msgc >= msgn) {msgc=0;}
  setl(msgc,virl);
  sett(msgc,virt);
  pause();
 }
}


function clientpause(tf)
{
 if (op) {return;}
 if (tf)
 {
  clearInterval(IID); clearTimeout(TID);
 } else {
  setz(msgc,2); pause(); return;
 }
 virl = 0; virt = msgh;
 setl(msgc,virl); sett(msgc,virt); setz(msgc,2);
 setl(msgcp,0); sett(msgcp,msgh); setz(msgcp,0);
}


function clientstep(stepd)
{
 if (op) {return;}
 if (stepd == 'back') {msgc-=1} else {msgc+=1;}
 if (msgc >= msgn) {msgc=0;}
 if (msgc < 0) {msgc=msgn-1;}
 for (cz=0; cz < msgn; cz++) {setl(cz,0-msgw); sett(cz,0); setz(cz,0); setv(msgc,false)};
 virl = 0; virt = msgh;
 setl(msgc,virl); sett(msgc,virt); setz(msgc,2); setv(msgc,true);
}

document.write('<style><!-- .marquee,.message {visibility: hide; position: absolute; z-index: 1; background-color: ' + bgcolor + '; layer-background-color: ' + bgcolor + ';} --></style>');
