<?php

$errors = array();

if (($_POST['form'] == "Submit") && ($_POST['ubform'] != "1"))
{
	foreach ($_POST as $name => $value)
	{
		$value = trim($value);
		$_POST[$name] = strip_tags($value);
	}

	if (empty($_POST['fname']))
	{
		$errors[] = "First name is required";
	}
	if (empty($_POST['lname']))
	{
		$errors[] = "Last name is required";
	}
	if (empty($_POST['address']))
	{
		$errors[] = "Address is required";
	}
	if (empty($_POST['email']))
	{
		$errors[] = "Email address is required";
	}
	if (!valid_email($_POST['email']))
	{
		$errors[] = "Invalid email address";
	}
	if (empty($_POST['affiliation']))
	{
		$errors[] = "An affiliation is required";
	}
	if ((!empty($_POST['subsidy'])) && (empty($_POST['subsidydetails'])))
	{
		$errors[] = "Please include details on why subsidy is needed";
	}
	if ((empty($_POST['developer'])) && (empty($_POST['userbusiness'])))
	{
		$errors[] = "You must select either the developer or user/business day (Linux@work) or both";
	}

	if (count($errors) == 0)
	{
		$fields = array('fname','lname','address','email','affiliation',
			'phone','fax','subtravel','subaccom','subsidydetails',
			'food','preconf','developer','userbusiness','foundation',
			'oldguadec','guadec2002','guadec2001','guadec2000','shareemail',
			'sharepostal');
		insert_data("register", $fields);
		
		if ($_POST['userbusiness'] == 1)
		{
			include "ub_form.php";
	
			do_header();
			?>
			<p>Your GU4DEC registration has been submitted.</p>
			<p>We encourage you to fill out the following additional questions for the Linux@work day. This form is optional, and information submitted may be used by the event sponsor, <a href="http://www.ltt.de/">LogOn Technology Transfer</a>.</p>
			<form method="post" action="<?=$_SERVER['PHP_SELF']?>">
			<?php
			print select_menu("Job Title/Function","jobtitle", $job_titles);
			print select_menu("Type Of Organization","orgtype", $org_types, 1);
			print checkboxes("Product Interest", $product_interests, 1);
			print select_menu("Company Size","compsize", $company_size);
			print select_menu("Purchasing Authority","purchauth", $purchasing_authority);
			print select_menu("Your Company IT/MIS Budget","compbudget", $company_budget);
			?>
			<br><br>
			<input type="hidden" name="email" value="<?=$_POST['email']?>">
			<input type="hidden" name="ubform" value="1">
			<input type="submit" name="form" value="Submit">
			</form>
			<?php
		    do_footer();
		}
		else
		{
			do_header();
			print "<h2 style=\"color: #0000bb; text-align: center;\">".
				"Your registration has been recorded.</h2>".
				"<p style=\"text-align: center;\"><img src=\"wilbur.png\" />".
				"<p style=\"text-align: center;\">See you there!</p>";
		    do_footer();
		}
	}
	else 
	{
		print "<p style=\"color: #CC0000\">";
		foreach ($errors as $error)
		{
			print "$error<br />";
		}
		print "</p>";
	}
}
elseif (($_POST['form'] == "Submit") && ($_POST['ubform'] == "1"))
{
	include "ub_form.php";

	$prod_str = "";
	foreach ($product_interests as $id => $value) {
		if (!empty($_POST[$id])) {
			$prod_str .= $id;
		}
	}
	if (!empty($prod_str)) {
		$_POST['product_interests'] = $prod_str;
	}

	$fields = array("jobtitle", "orgtype", "orgtype_other", 
					"product_interests", "product_interests_other", "compsize",
					"purchauth", "compbudget","email");
	insert_data("linuxatwork", $fields);

	do_header();
	print "<p>Thanks for your time!</p><p>See you at Linux@work 2003!</p>";
	do_footer();
}
else
{
	do_header();
?>

<?php
	if (count($errors) != 0)
	{
		print "<p style=\"color: #CC0000\">";
		foreach ($errors as $error)
		{
			print "$error<br />";
		}
		print "</p>";
	}
?>

<p><span class="red">*</span> - required field</p>

<form method="post" action="<?=$_SERVER['PHP_SELF']?>">
<p><b>General</b></p>
<table>
<tr>
	<td>First Name: <span class="red">*</span></td>
	<td><input type="text" name="fname" value="<?=$_POST['fname']?>"></td>
</tr>
<tr>
	<td>Last Name: <span class="red">*</span></td>
	<td><input type="text" name="lname" value="<?=$_POST['lname']?>"></td>
</tr>
<tr>
	<td valign="top">Address: <span class="red">*</span></td>
	<td><textarea name="address"><?=$_POST['address']?></textarea></td>
</tr>
<tr>
	<td>Email: <span class="red">*</span></td>
	<td><input type="text" name="email" value="<?=$_POST['email']?>"></td>
</tr>
<tr>
	<td>Affiliation: <span class="red">*</span></td>
	<td><input type="text" name="affiliation" value="<?=$_POST['affiliation']?>"> (Name of company, university, etc)</td>
</tr>
<tr>
	<td>Telephone:</td>
	<td><input type="text" name="phone" value="<?=$_POST['phone']?>"></td>
</tr>
<tr>
	<td>Fax:</td>
	<td><input type="text" name="fax" value="<?=$_POST['fax']?>"></td>
</tr>
</table>

<p><b>Subsidy</b></p>
<input type="checkbox" name="subtravel" value="1" <?php if ($_POST['subtravel'] == "1") { print "checked"; } ?>> I require travel subsidy<br />
<input type="checkbox" name="subaccom" value="1" <?php if ($_POST['subaccom'] == "1") { print "checked"; } ?>> I require accommodation subsidy<br /><br />
Details (e.g. how much):<br />
<textarea cols="50" rows="5" name="subsidydetails"><?=$_POST['subsidydetails']?></textarea><br />

<p><b>Dietary Requirements</b> (Vegetarian, Kosher, etc.)</p>
<textarea cols="50" rows="5" name="food"><?=$_POST['food']?></textarea><br />

<p><b>Attendence</b> <span class="red">*</span> (must select at least one)</p>
<input type="checkbox" name="preconf" value="1" <?php if ($_POST['preconf'] == "1") { print "checked"; } ?>> I will attend the pre-conference weekend<br />
<input type="checkbox" name="developer" value="1" <?php if ($_POST['developer'] == "1") { print "checked"; } ?>> I will attend the developer conference<br />
<input type="checkbox" name="userbusiness" value="1" <?php if ($_POST['userbusiness'] == "1") { print "checked"; } ?>> I will attend the user/business day (Linux@work) on June 18<br /><br />
<div style="font-size: smaller">Please indicate the conference days you wish to attend, as this allows us to plan ahead.</div>
<div style="font-size: smaller">See <a href="schedule/">GU4DEC Schedule</a> for details on the events.</div>
<br />

<p><b>GNOME Involvement</b></p>
<input type="checkbox" name="foundation" value="1" <?php if ($_POST['foundation'] == "1") { print "checked"; } ?>> I am a member of the GNOME Foundation<br />
<input type="checkbox" name="oldguadec" value="1" <?php if ($_POST['oldguadec'] == "1") { print "checked"; } ?>> I have attended previous GUADEC conferences<br />
<div style="padding-left: 20px">
<input type="checkbox" name="guadec2002" value="1" <?php if ($_POST['guadec2002'] == "1") { print "checked"; } ?>> 2002 Seville<br />
<input type="checkbox" name="guadec2001" value="1" <?php if ($_POST['guadec2001'] == "1") { print "checked"; } ?>> 2001 Copenhagen<br />
<input type="checkbox" name="guadec2000" value="1" <?php if ($_POST['guadec2000'] == "1") { print "checked"; } ?>> 2000 Paris<br />
</div>

<p><b>Information Sharing</b></p>
<div>You may share the following information with sponsoring companies:</div>
<input type="checkbox" name="shareemail" value="1" <?php if ($_POST['shareemail'] == "1") { print "checked"; } ?>> Email address<br />
<input type="checkbox" name="sharepostal" value="1" <?php if ($_POST['sharepostal'] == "1") { print "checked"; } ?>> Postal mail address<br />

<br />

<input type="submit" name="form" value="Submit">
</form>

<p style="font-size: smaller">Privacy Policy: Information collected will be used for GUADEC registration only unless you have specified otherwise.</p>

<?php
  do_footer();
}

function do_header() 
{
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html>

<head>
	<title>
	GU4DEC: Registration
	</title>
	<link rel="icon" type="image/png" href="img/gnome-16.png" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="default.css" />
	<link rel="stylesheet" type="text/css" href="guadec.css" />
	<style type="text/css">
	#hdr {
		background: url("img/header-bg-hills01.png") no-repeat;
		background-color: #e3ffc3;
	}
	</style>
</head>

<body>
<div id="body">

<h1>Registration</h1>

<?php
}

function do_footer()
{
?>
</div>

<div id="sidebar">
	<p><a href="register.php">Register NOW</a> for GU4DEC 2003!</p>

	<p>Find out what's happening! See the <a href="schedule/">
	conference schedule and speakers list</a>.</p>

	<p>Your <a href="guide/">traveller's guide to
	GU4DEC</a>: all about Trinity College and Dublin, Ireland.</p>
	
	<p>If you think your company could benefit from GU4DEC, please help us to
	help you: <a href="sponsor/">become a GU4DEC
	Sponsor</a>.</p>

	<p>Read more about GU4DEC and find official announcements in our <a
	href="press/">Press Area</a>.</p>

	<p>Need more help? Subscribe to the <a
	href="http://mail.gnome.org/mailman/listinfo/guadec-list">GU4DEC mailing
	list</a> or <a href="mailto:guadec-feedback@gnome.org">email the conference
	organisers</a>.</p>
</div>

<div id="hdr">
	<a href="http://www.guadec.org/"><img id="logo" src="img/gnome-64" alt="Home" title="Back to the GUADEC home page"/></a>
	<p class="none"></p>
	<div id="hdrNav">
		<a href="http://www.gnome.org/intro/findout.html">About GNOME</a> &middot;
		<a href="http://www.gnome.org/start/2.2/">Download</a> &middot;
		<!--<a href="http://www.gnome.org/contribute/"><i>Get Involved!</i></a> &middot;-->
		<a href="http://www.gnome.org/">Users</a> &middot;
		<a href="http://developer.gnome.org/">Developers</a> &middot;
		<a href="http://foundation.gnome.org/">Foundation</a> &middot;
		<a href="mailto:webmaster@gnome.org">Contact</a>
	</div>
</div>

<div id="copyright">
Copyright &copy; 2003, <a href="http://www.gnome.org/">The GNOME Project</a>.<br />
Optimised for <a href="http://www.w3.org/">standards</a>.
Hosted by <a href="http://www.redhat.com/">Red Hat</a>.
</div>

</body>

</html>
<?php
}

function select_menu($title, $name, $values, $other = 0) {
	$ret = "<br><div><strong>$title</strong></div><select name=\"$name\">\n".
		"<option value=\"0\" selected=\"selected\"> Please select one\n";	
	foreach ($values as $id => $value) {
		$ret .= "<option value=\"$id\">$value\n";
	}
	$ret .= "</select>\n<br>\n";
	if ($other) {
		$oname = $name."_other";
		$ret .= "<br>Other: <input type=\"text\" name=\"$oname\" size=\"25\" maxsize=\"50\"><br>\n";
	}
	return $ret;
}

function checkboxes($title, $values, $other = 0) {
	$ret = "<br><div><strong>$title</strong></div>\n";
	foreach ($values as $id => $value) {
		$ret .= "<input type=\"checkbox\" name=\"$id\"> $value<br>\n";
	}
	if ($other) {
		$ret .= "<br>Other: <input type=\"text\" name=\"product_interests_other\" size=\"25\" maxsize=\"50\"><br>\n";
	}
	return $ret;
}

/* Taken from phpbuilder.net */
function valid_email ($email) {
  list($local, $domain) = explode("@", $email);

  $pattern_local = '^([0-9a-z]*([-|_]?[0-9a-z]+)*)(([-|_]?)\.([-|_]?)[0-9a-z]*([-|_]?[0-9a-z]+)+)*([-|_]?)$';
  $pattern_domain = '^([0-9a-z]+([-]?[0-9a-z]+)*)(([-]?)\.([-]?)[0-9a-z]*([-]?[0-9a-z]+)+)*\.[a-z]{2,4}$';

  $match_local = eregi($pattern_local, $local);
  $match_domain = eregi($pattern_domain, $domain);
	
  if ($match_local && $match_domain) {
    return 1;
  } else {
    return 0;
  }
}

function insert_data ($table, $fields) {
	#include "connect.php";
	include "/home/admin/applist/private.php3";
	mysql_select_db("guadec");

	$query = "INSERT INTO $table SET ";

	foreach ($fields as $field)
	{
		if ((isset($_POST[$field])) && (!empty($_POST[$field])))
		{
			$query .= "$field=\"".$_POST[$field]."\", ";
		}
	}

	$query = substr_replace($query, "", -2);

	$res = mysql_query($query);
	$count = mysql_affected_rows();

	if ($count < 1) {
		print "Database update failed";
		exit;
	} else {
		return $count;
	}
}

?>
